# coding:utf-8
'''
Created on 2021年4月3日

@author: star
'''

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from Common.Config import Logs
from Common.Config import Config

class UICommon(object):
    def __init__(self):
        self.browser_type = None      #浏览器类型
        
    '''打开浏览器的方法'''
    def Openbrowser(self, browser_type, url):
        Config.initialCondition(self)       #刷新配置文件路径
        if browser_type == "Firefox":
            self.driver = webdriver.Firefox()
            self.driver.get(url)
        elif browser_type == "Chrome" or browser_type == "":
            self.driver = webdriver.Chrome()
            self.driver.get(url)
        elif browser_type == "IE":
            self.driver = webdriver.Ie()
            self.driver.get(url)
        self.driver.maximize_window()
    
    '''获取web的属性'''        
    def WebAttribute(self, attribute):
        if attribute == "url":
            self.driver.current_url     #获取页面地址
        elif attribute == "page_source":
            self.driver.page_source     #获取页面代码
        elif attribute == "title":
            self.driver.title   #获取页面标题
        elif attribute == "forward":
            self.driver.forward()   #页面前进
        elif attribute == "back":
            self.driver.back()  #页面后退
        elif attribute == "close":
            self.driver.close()  #页面关闭      
        elif attribute == "quit":
            self.driver.quit()  #关闭浏览器
        elif attribute == "handle":
            self.driver.window_handles  #获取所有窗口句柄      
        elif attribute == "all_handle":
            self.driver.window_handles  #获取所有窗口句柄
        elif attribute == "max":
            self.driver.maximize_window     #窗口最大化
        elif attribute == "refresh":
            self.driver.refresh()   #页面刷新
        elif attribute == "name":
            self.driver.name()   #获取浏览器名称   
            
    ''''隐式等待'''
    def wait(self, seconds):
        self.driver.implicitly_wait(seconds)
        
    ''''显性等待时间'''
    def WebDriverWait(self, methon , type, value, time, text):
        if methon == "clickable":
            #元素可见并且可操作
            if type == "id":
                WebDriverWait(self.driver, time).until(expected_conditions.element_to_be_clickable((By.ID, value)))
            elif type == "name":
                WebDriverWait(self.driver, time).until(expected_conditions.element_to_be_clickable((By.NAME, value)))  
            elif type == "class":
                WebDriverWait(self.driver, time).until(expected_conditions.element_to_be_clickable((By.CLASS_NAME, value))) 
            elif type == "xpath" or type == "":
                WebDriverWait(self.driver, time).until(expected_conditions.element_to_be_clickable((By.XPATH, value))) 
            elif type == "link_text":
                WebDriverWait(self.driver, time).until(expected_conditions.element_to_be_clickable((By.LINK_TEXT, value))) 
            elif type == "partial_link_text":
                WebDriverWait(self.driver, time).until(expected_conditions.element_to_be_clickable((By.PARTIAL_LINK_TEXT, value))) 
            elif type == "selector":
                WebDriverWait(self.driver, time).until(expected_conditions.element_to_be_clickable((By.CSS_SELECTOR, value))) 
    
        elif methon == "text":
            #指定元素的文本位置,一般用于验证一个文本信息或错误的提示信息
            if type == "id":
                WebDriverWait(self.driver, time).until(expected_conditions.text_to_be_present_in_element((By.ID, value, text)))
            elif type == "name":
                WebDriverWait(self.driver, time).until(expected_conditions.text_to_be_present_in_element((By.NAME, value, text)))  
            elif type == "class":
                WebDriverWait(self.driver, time).until(expected_conditions.text_to_be_present_in_element((By.CLASS_NAME, value, text))) 
            elif type == "xpath" or type == "":
                WebDriverWait(self.driver, time).until(expected_conditions.text_to_be_present_in_element((By.XPATH, value, text))) 
            elif type == "link_text":
                WebDriverWait(self.driver, time).until(expected_conditions.text_to_be_present_in_element((By.LINK_TEXT, value, text))) 
            elif type == "partial_link_text":
                WebDriverWait(self.driver, time).until(expected_conditions.text_to_be_present_in_element((By.PARTIAL_LINK_TEXT, value, text))) 
            elif type == "selector":
                WebDriverWait(self.driver, time).until(expected_conditions.text_to_be_present_in_element((By.CSS_SELECTOR, value, text))) 
     
        elif methon == "visibility":
            #判断元素是否可见
            if type == "id":
                WebDriverWait(self.driver, time).until(expected_conditions.visibility_of_element_located((By.ID, value)))
            elif type == "name":
                WebDriverWait(self.driver, time).until(expected_conditions.visibility_of_element_located((By.NAME, value)))  
            elif type == "class":
                WebDriverWait(self.driver, time).until(expected_conditions.visibility_of_element_located((By.CLASS_NAME, value))) 
            elif type == "xpath" or type == "":
                WebDriverWait(self.driver, time).until(expected_conditions.visibility_of_element_located((By.XPATH, value))) 
            elif type == "link_text":
                WebDriverWait(self.driver, time).until(expected_conditions.visibility_of_element_located((By.LINK_TEXT, value))) 
            elif type == "partial_link_text":
                WebDriverWait(self.driver, time).until(expected_conditions.visibility_of_element_located((By.PARTIAL_LINK_TEXT, value))) 
            elif type == "selector":
                WebDriverWait(self.driver, time).until(expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, value))) 

    '''针对iframe框架的定位'''    
    def Iframe(self, type, value, num):
        if type == "enter":
            if value != "" and num == "":
                self.driver.switch_to_frame(value)    #进入iframe框架,通过id定位
            else:
                self.driver.switch_to_frame(num)    #进入iframe框架,通过索引定位
        elif type == "out":
            self.driver.switch_to_default_content()    #跳出iframe框架
        
    '''用单个元素的方式检查元素是否存在'''
    def Check(self, type, value, text):
        if type == "id":   
            content = self.driver.find_element_by_id(value).text    #通过id定位
            self.assertEqual(content, text)
        elif type == "name":
            content = self.driver.find_element_by_name(value).text    #通过name定位
            self.assertEqual(content, text)
        elif type == "class_name":
            content = self.driver.find_element_by_class_name(value).text    #通过class定位
            self.assertEqual(content, text)
        elif type == "xpath" or type == "":
            content = self.driver.find_element_by_xpath(value).text    #通过xpath定位
            self.assertEqual(content, text)
        elif type == "link_text":
            content = self.driver.find_element_by_link_text(value).text    #通过超链接定位
            self.assertEqual(content, text)
        elif type == "partial_link_text":
            content = self.driver.find_element_by_partial_link_text(value).text    #通过超链接模糊定位
            self.assertEqual(content, text)
        elif type == "css_selector":
            content = self.deiver.find_element_by_css_selector(value).text     #通过selector对超链接模糊定位
            self.assertEqual(content, text)
        Logs().info(u"元素位置：%s"%value)
        Logs().info(u"元素内容：%s"%content)
        Config.screenShot(self)
            
    '''用多个元素的方式验证元素是否存在'''
    def Checks(self, type, value, num, text):
        if type == "id":
            content = self.driver.find_element_by_id(value)[num].text    #通过id[]索引的方式定位
            self.assertEqual(content, text)
        elif type == "tag_name":
            content = self.driver.find_element_by_tag_name(value)[num].text     #通过前端代码(例如：input)标签索引[]的方式定位
            self.assertEqual(content, text)
        Logs().info(u"元素位置：%s"%value)
        Logs().info(u"元素内容：%s"%content)
        Config.screenShot(self)
            
            
    '''清空元素中输入的关键字'''
    def clear(self, type, value):
        if type == "id":   
            self.driver.find_element_by_id(value).clear()    #通过id定位
        elif type == "name":
            self.driver.find_element_by_name(value).clear()   #通过name定位
        elif type == "class_name":
            self.driver.find_element_by_class_name(value).clear()    #通过class定位
        elif type == "xpath" or type == "":
            self.driver.find_element_by_xpath(value).clear()    #通过xpath定位
        elif type == "link_text":
            self.driver.find_element_by_link_text(value).clear()    #通过超链接定位
        elif type == "partial_link_text":
            self.driver.find_element_by_partial_link_text(value).clear()    #通过超链接模糊定位
        elif type == "css_selector":
            self.deiver.find_element_by_css_selector(value).clear()     #通过selector对超链接模糊定位
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)
    
    '''获取元素属性值'''
    def get_attribute(self, type, value, attribute):      #method为输入框的元素属性"placeholder"和"value"
        if type == "id":   
            self.driver.find_element_by_id(value).get_attribute(attribute)    #通过id定位
        elif type == "name":
            self.driver.find_element_by_name(value).get_attribute(attribute)   #通过name定位
        elif type == "class_name":
            self.driver.find_element_by_class_name(value).get_attribute(attribute)    #通过class定位
        elif type == "xpath" or type == "":
            self.driver.find_element_by_xpath(value).get_attribute(attribute)    #通过xpath定位
        elif type == "link_text":
            self.driver.find_element_by_link_text(value).get_attribute(attribute)    #通过超链接定位
        elif type == "partial_link_text":
            self.driver.find_element_by_partial_link_text(value).get_attribute(attribute)    #通过超链接模糊定位
        elif type == "css_selector":
            self.deiver.find_element_by_css_selector(value).get_attribute(attribute)     #通过selector对超链接模糊定位
        
    '''检查元素是否可见'''    
    def is_displayed(self, type, value):
        if type == "id":   
            self.driver.find_element_by_id(value).is_displayed()    #通过id定位
        elif type == "name":
            self.driver.find_element_by_name(value).is_displayed()    #通过name定位
        elif type == "class_name":
            self.driver.find_element_by_class_name(value).is_displayed()    #通过class定位
        elif type == "xpath" or type == "":
            self.driver.find_element_by_xpath(value).is_displayed()    #通过xpath定位
        elif type == "link_text":
            self.driver.find_element_by_link_text(value).is_displayed()    #通过超链接定位
        elif type == "partial_link_text":
            self.driver.find_element_by_partial_link_text(value).is_displayed()    #通过超链接模糊定位
        elif type == "css_selector":
            self.deiver.find_element_by_css_selector(value).is_displayed()     #通过selector对超链接模糊定位
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)
        
    '''检查元素是否可编辑'''    
    def is_enabled(self, type, value):
        if type == "id":   
            self.driver.find_element_by_id(value).is_enabled()    #通过id定位
        elif type == "name":
            self.driver.find_element_by_name(value).is_enabled()    #通过name定位
        elif type == "class_name":
            self.driver.find_element_by_class_name(value).is_enabled()    #通过class定位
        elif type == "xpath" or type == "":
            self.driver.find_element_by_xpath(value).is_enabled()    #通过xpath定位
        elif type == "link_text":
            self.driver.find_element_by_link_text(value).is_enabled()    #通过超链接定位
        elif type == "partial_link_text":
            self.driver.find_element_by_partial_link_text(value).is_enabled()    #通过超链接模糊定位
        elif type == "css_selector":
            self.deiver.find_element_by_css_selector(value).is_enabled()     #通过selector对超链接模糊定位
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)
    
    '''检查元素是否已选中'''    
    def is_selected(self, type, value):
        if type == "id":   
            self.driver.find_element_by_id(value).is_selected()    #通过id定位
        elif type == "name":
            self.driver.find_element_by_name(value).is_selected()    #通过name定位
        elif type == "class_name":
            self.driver.find_element_by_class_name(value).is_selected()    #通过class定位
        elif type == "xpath" or type == "":
            self.driver.find_element_by_xpath(value).is_selected()    #通过xpath定位
        elif type == "link_text":
            self.driver.find_element_by_link_text(value).is_selected()    #通过超链接定位
        elif type == "partial_link_text":
            self.driver.find_element_by_partial_link_text(value).is_selected()    #通过超链接模糊定位
        elif type == "css_selector":
            self.deiver.find_element_by_css_selector(value).is_selected()     #通过selector对超链接模糊定位
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)
        
    '''提交表单'''
    def Submit(self, type, value):
        if type == "id":   
            self.driver.find_element_by_id(value).submit()    #通过id定位
        elif type == "name":
            self.driver.find_element_by_name(value).submit()    #通过name定位
        elif type == "class_name":
            self.driver.find_element_by_class_name(value).submit()    #通过class定位
        elif type == "xpath" or type == "":
            self.driver.find_element_by_xpath(value).submit()    #通过xpath定位
        elif type == "link_text":
            self.driver.find_element_by_link_text(value).submit()    #通过超链接定位
        elif type == "partial_link_text":
            self.driver.find_element_by_partial_link_text(value).submit()    #通过超链接模糊定位
        elif type == "css_selector":
            self.deiver.find_element_by_css_selector(value).submit()     #通过selector对超链接模糊定位
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)
    
    '''下拉框定位'''    
    def Select(self, type, value, method, num):
        if method == "index":   #索引定位
            if type == "id":   
                Select(self.driver.find_element_by_id(value)).select_by_index(num)    #通过id定位  
            elif type == "name":
                Select(self.driver.find_element_by_name(value)).select_by_index(num)    #通过name定位
            elif type == "class_name":
                Select(self.driver.find_element_by_class_name(value)).select_by_index(num)    #通过class定位
            elif type == "xpath" or type == "":
                Select(self.driver.find_element_by_xpath(value)).select_by_index(num)    #通过xpath定位
            elif type == "link_text":
                Select(self.driver.find_element_by_link_text(value)).select_by_index(num)    #通过超链接定位
            elif type == "partial_link_text":
                Select(self.driver.find_element_by_partial_link_text(value)).select_by_index(num)    #通过超链接模糊定位
            elif type == "css_selector":
                Select(self.driver.find_element_by_css_selector(value)).select_by_index(num)      #通过selector对超链接模糊定位 
        elif method == "value":     #value定位  
            if type == "id":   
                Select(self.driver.find_element_by_id(value)).select_by_value(num)    #通过id定位 
            elif type == "name":
                Select(self.driver.find_element_by_name(value)).select_by_value(num)     #通过name定位
            elif type == "class_name":
                Select(self.driver.find_element_by_class_name(value)).select_by_value(num)     #通过class定位
            elif type == "xpath" or type == "":
                Select(self.driver.find_element_by_xpath(value)).select_by_value(num)    #通过xpath定位 
            elif type == "link_text":
                Select(self.driver.find_element_by_link_text(value)).select_by_value(num)    #通过超链接定位 
            elif type == "partial_link_text":
                Select(self.driver.find_element_by_partial_link_text(value)).select_by_value(num)    #通过超链接模糊定位 
            elif type == "css_selector":
                Select(self.driver.find_element_by_css_selector(value)).select_by_value(num)      #通过selector对超链接模糊定位
        elif method == "text":      #文本定位
            if type == "id":   
                Select(self.driver.find_element_by_id(value)).select_by_visible_text(num)    #通过id定位
            elif type == "name":
                Select(self.driver.find_element_by_name(value)).select_by_visible_text(num)    #通过name定位
            elif type == "class_name":
                Select(self.driver.find_element_by_class_name(value)).select_by_visible_text(num)    #通过class定位
            elif type == "xpath" or type == "":
                Select(self.driver.find_element_by_xpath(value)).select_by_visible_text(num)    #通过xpath定位
            elif type == "link_text":
                Select(self.driver.find_element_by_link_text(value)).select_by_visible_text(num)    #通过超链接定位
            elif type == "partial_link_text":
                Select(self.driver.find_element_by_partial_link_text(value)).select_by_visible_text(num)    #通过超链接模糊定位
            elif type == "css_selector":
                Select(self.driver.find_element_by_css_selector(value)).select_by_visible_text(num)     #通过selector对超链接模糊定位
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)  
            
    '''弹出框'''
    def Popup(self, type):
        if type == "alert":
            self.driver.switch_to_alert()   #警告框,点击“确定”按钮调用的方法是.accept()
            Logs().info("警告框")
        elif type == "confirm":
            self.driver.switch_to_alert()   #确认框,点击“确定”按钮调用的方法是.accept(),点击“取消”按钮调用的方法是.dismiss()
            Logs().info("确认框")
        elif type == "prompt":
            self.driver.switch_to_alert()   #消息对话框,在对话框中输入信息使用的方法是.send_keys()
            Logs().info("消息对话框")
        Config.screenShot(self)
            
    '''鼠标事件'''
    def ActionChains(self, methon, type, value):
        if methon == "move" or methon == "":
            if type == "id":   
                element = self.driver.find_element_by_id(value)    #通过id定位
            elif type == "name":
                element = self.driver.find_element_by_name(value)    #通过name定位
            elif type == "class_name":
                element = self.driver.find_element_by_class_name(value)    #通过class定位
            elif type == "xpath" or type == "":
                element = self.driver.find_element_by_xpath(value)    #通过xpath定位
            elif type == "link_text":
                element = self.driver.find_element_by_link_text(value)    #通过超链接定位
            elif type == "partial_link_text":
                element = self.driver.find_element_by_partial_link_text(value)    #通过超链接模糊定位
            elif type == "css_selector":
                element = self.deiver.find_element_by_css_selector(value)    #通过selector对超链接模糊定位
            ActionChains(self.driver).move_to_element(element).perform()     #鼠标悬浮
        elif methon == "content":
            if type == "id":   
                element = self.driver.find_element_by_id(value)    #通过id定位
            elif type == "name":
                element = self.driver.find_element_by_name(value)    #通过name定位
            elif type == "class_name":
                element = self.driver.find_element_by_class_name(value)    #通过class定位
            elif type == "xpath" or type == "":
                element = self.driver.find_element_by_xpath(value)    #通过xpath定位
            elif type == "link_text":
                element = self.driver.find_element_by_link_text(value)    #通过超链接定位
            elif type == "partial_link_text":
                element = self.driver.find_element_by_partial_link_text(value)    #通过超链接模糊定位
            elif type == "css_selector":
                element = self.deiver.find_element_by_css_selector(value)    #通过selector对超链接模糊定位)
            ActionChains(self.driver).context_click(element).perform()     #鼠标右击
        elif methon == "double":
            if type == "id":   
                element = self.driver.find_element_by_id(value)    #通过id定位
            elif type == "name":
                element = self.driver.find_element_by_name(value)    #通过name定位
            elif type == "class_name":
                element = self.driver.find_element_by_class_name(value)    #通过class定位
            elif type == "xpath" or type == "":
                element = self.driver.find_element_by_xpath(value)    #通过xpath定位
            elif type == "link_text":
                element = self.driver.find_element_by_link_text(value)    #通过超链接定位
            elif type == "partial_link_text":
                element = self.driver.find_element_by_partial_link_text(value)    #通过超链接模糊定位
            elif type == "css_selector":
                element = self.deiver.find_element_by_css_selector(value)    #通过selector对超链接模糊定位)
            ActionChains(self.driver).double_click(element).perform()     #鼠标双击
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)
    
    '''键盘事件'''        
    def Keys(self, type, value, key, name):
        if key == "ctrl":     #ctrl+name
            if type == "id":   
                self.driver.find_element_by_id(value).send_Keys(Keys.CANCEL, name)    #通过id定位
            elif type == "name":
                self.driver.find_element_by_name(value).send_Keys(Keys.CANCEL, name)    #通过name定位
            elif type == "class_name":
                self.driver.find_element_by_class_name(value).send_Keys(Keys.CANCEL, name)    #通过class定位
            elif type == "xpath" or type == "":
                self.driver.find_element_by_xpath(value).send_Keys(Keys.CANCEL, name)    #通过xpath定位
            elif type == "link_text":
                self.driver.find_element_by_link_text(value).send_Keys(Keys.CANCEL, name)    #通过超链接定位
            elif type == "partial_link_text":
                self.driver.find_element_by_partial_link_text(value).send_Keys(Keys.CANCEL, name)    #通过超链接模糊定位
            elif type == "css_selector":
                self.deiver.find_element_by_css_selector(value) .send_Keys(Keys.CANCEL, name)    #通过selector对超链接模糊定位
        elif key == "backspace":    #退格键
            if type == "id":   
                self.driver.find_element_by_id(value).send_Keys(Keys.BACKSPACE)    #通过id定位
            elif type == "name":
                self.driver.find_element_by_name(value).send_Keys(Keys.BACKSPACE)    #通过name定位
            elif type == "class_name":
                self.driver.find_element_by_class_name(value).send_Keys(Keys.BACKSPACE)    #通过class定位
            elif type == "xpath" or type == "":
                self.driver.find_element_by_xpath(value).send_Keys(Keys.BACKSPACE)    #通过xpath定位
            elif type == "link_text":
                self.driver.find_element_by_link_text(value).send_Keys(Keys.BACKSPACE)    #通过超链接定位
            elif type == "partial_link_text":
                self.driver.find_element_by_partial_link_text(value).send_Keys(Keys.BACKSPACE)    #通过超链接模糊定位
            elif type == "css_selector":
                self.deiver.find_element_by_css_selector(value).send_Keys(Keys.BACKSPACE)     #通过selector对超链接模糊定位
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)
            
    '''JavaScript的处理,界面滑动'''
    def Slip(self, range):
        #界面上下滑动 
        js = "var q=document.documentElement.scrollTop=%s"%range
        self.driver.execute_script(js) 
    
    '''JavaScript的处理,往富文本框中写入内容'''
    def richText(self, value, text):
        js = "document.getElementById("+value+").contentWindow.document.body.innerHTML= '{0}'".format(text)
        self.driver.execute_script(js)
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)
        
    '''JavaScript的处理,时间控件的处理'''
    def timeJs(self , value, time):
        js = "$(\"input[placeholder="+value+"]\").removeAttr('readonly');"\
             "$(\"input[placeholder="+value+"]\").attr('value','0')".format(time)
        self.driver.execute_script(js) 
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)
        
    '''点击事件'''
    def Click(self, type, value):
        if type == "id":   
            self.driver.find_element_by_id(value).click()    #通过id定位
        elif type == "name":
            self.driver.find_element_by_name(value).click()    #通过name定位
        elif type == "class_name":
            self.driver.find_element_by_class_name(value).click()    #通过class定位
        elif type == "xpath" or type == "":
            self.driver.find_element_by_xpath(value).click()    #通过xpath定位
        elif type == "link_text":
            self.driver.find_element_by_link_text(value).click()    #通过超链接定位
        elif type == "partial_link_text":
            self.driver.find_element_by_partial_link_text(value).click()    #通过超链接模糊定位
        elif type == "css_selector":
            self.deiver.find_element_by_css_selector(value).click()     #通过selector对超链接模糊定位
        Logs().info(u"元素位置：%s"%value)
        Config.screenShot(self)
        