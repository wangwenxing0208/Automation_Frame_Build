# conding:utf-8
# -*- coding: cp936 -*-
'''
Created on 2021年4月4日

@author: star
'''

import unittest, time
from UIFrame.UICommon import UICommon

class Test03(unittest.TestCase):
    '''测试套件,按顺序执行'''
    def setUp(self):
        UICommon.Openbrowser(self, browser_type = "", url = "http://120.133.27.66/wbalone/index.html")

    def tearDown(self):
        UICommon.WebAttribute(self, attribute = "quit")   

    def testName(self):
        time.sleep(3)
        UICommon.Check(self, type = '', value = '//*[@id="loginBtn"]')
        
    def testName1(self):
        time.sleep(3)
        UICommon.Check(self, type = '', value = '//*[@id="loginBtn1"]')

if __name__ == "__main__":
    suite=unittest.TestSuite()
    suite.addTest(Test03("testName1"))
    suite.addTest(Test03("testName"))
    unittest.TextTestRunner(verbosity = 2).run(suite)
    