# conding:utf-8
# -*- coding: cp936 -*-
'''
Created on 2021年4月4日

@author: star
'''

import unittest, time
from UIFrame.UICommon import UICommon

class Test02(unittest.TestCase):
    '''测试固件只执行一次'''
    @classmethod
    def setUpClass(cls):
        UICommon.Openbrowser(cls, browser_type = "", url = "http://120.133.27.66/wbalone/index.html")

    @classmethod
    def tearDownClass(cls):
        UICommon.WebAttribute(cls, attribute = "quit")   

    def testName(self):
        time.sleep(3)
        UICommon.Check(self, type = '', value = '//*[@id="loginBtn"]', text = '登 录')
        
#     def testname1(self):
#         time.sleep(3)
#         UICommon.Check(self, type = '', value = '//*[@id="loginBtn"]', text = '登 录1')

if __name__ == "__main__":
    unittest.main(verbosity = 2)