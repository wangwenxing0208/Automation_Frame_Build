# coding:utf-8
'''
Created on 2021年4月4日

@author: star
'''

import configparser,time,os,logging

class Config(object):
    '''初始条件'''
    def initialCondition(self):
        '''配置文件路径'''
#         Config = "C:\eclipse-workspace\Automation_Frame_Build\Config_File\Config.cof"
        path0 = os.path.join(os.path.dirname(__file__),'')
        path = path0[:-7] + 'Config_File'
        Config = path + "\Config.cof"
        '''ConfigParser是configparser中的一个类,实例化一个对象'''
        conf = configparser.ConfigParser()
        '''read是ConfigParser类中一个方法 ,读取配置文件路径'''
        conf.read(Config, encoding='UTF-8')
        '''get是ConfigParser类中一个方法 ,读取配置文件中的report_address路径'''
        report_address = conf.get("address", "report_address")
        '''获取当前时间'''
        now = time.strftime("%Y-%m-%d-%H_%M_%S", time.localtime(time.time()))
        '''创建一个新的report_address字符串'''
        report_address = report_address + now
        '''创建一个report_address路径'''
        os.makedirs(report_address)
        '''创建一个report_logs字符串'''
        report_logs = report_address+"\logs"
        '''创建一个report_logs路径'''
        os.makedirs(report_logs)
        '''创建一个report_image字符串'''
        report_image = report_address+"\image"
        '''创建一个report_image路径'''
        os.makedirs(report_image)
        '''set是ConfigParser类中一个方法 ,更新report_address_1路径'''
        conf.set(section="address", option="report_address_1", value=report_address)
        '''set是ConfigParser类中一个方法 ,更新address_logs路径'''
        conf.set(section="address", option="address_logs", value=report_logs)
        '''set是ConfigParser类中一个方法 ,更新address_mages路径'''
        conf.set(section="address", option="address_mages", value=report_image)
        '''打开Config.cof文件'''
        f = open(Config, "w", encoding='UTF-8')
        '''向Config.cof中写入文件'''
        conf.write(f)
        '''关闭Config.cof文件'''
        f.close()
        
    '''截图存放到存储位置'''
    def screenShot(self):
        '''配置文件路径'''
#         Config = "C:\eclipse-workspace\Automation_Frame_Build\Config_File\Config.cof"
        path0 = os.path.join(os.path.dirname(__file__),'')
        path = path0[:-7] + 'Config_File'
        Config = path + "\Config.cof"
        '''ConfigParser是configparser中的一个类,实例化一个对象'''
        conf = configparser.ConfigParser()
        '''read是ConfigParser类中一个方法 ,读取配置文件路径'''
        conf.read(Config, encoding = 'UTF-8')
        '''get是ConfigParser类中一个方法 ,读取配置文件中的address_mages路径'''
        address_mages = conf.get("address", "address_mages")
        '''获取当前时间'''
        nowTime = time.strftime("%Y-%m-%d-%H_%M_%S", time.localtime())
        '''截图并存放到存储位置'''
        self.driver.get_screenshot_as_file(address_mages + "//" +nowTime+ ".png")
        
'''创建一个Logs类，日志'''        
class Logs(object):
    
    def __init__(self):
        '''配置文件路径'''
#         Config = "C:\eclipse-workspace\Automation_Frame_Build\Config_File\Config.cof"
        path0 = os.path.join(os.path.dirname(__file__),'')
        path = path0[:-7] + 'Config_File'
        Config = path + "\Config.cof"
        '''ConfigParser是configparser中的一个类,实例化一个对象'''
        conf = configparser.ConfigParser()
        '''read是ConfigParser类中一个方法 ,读取配置文件路径'''
        conf.read(Config, encoding='UTF-8')
        '''get是ConfigParser类中一个方法 ,读取配置文件中的address_logs路径'''
        log_path = conf.get("address", "address_logs")
        '''创建一个新的log_path字符串'''
        log_path = log_path + "//"
        '''给日志命名'''
        self.logname = os.path.join(log_path, '%s.log'%time.strftime('%Y_%m_%d'))
        '''初始化日志内容'''
        self.logger = logging.getLogger()
        '''设置logger等级'''
        self.logger.setLevel(logging.DEBUG)
        '''设置日志输出格式'''
        self.formatter = logging.Formatter('[%(asctime)s] - %(levelname)s: %(message)s')
    
    def __console(self, level, message):
        '''创建一个FileHandler，用于写到本地'''
        fh = logging.FileHandler(self.logname, 'a', encoding='utf-8')
        '''设置logger等级'''
        fh.setLevel(logging.DEBUG)
        '''日志输出格式'''
        fh.setFormatter(self.formatter)
        '''给logger添加handler'''
        self.logger.addHandler(fh)
        
        ''' 创建一个StreamHandler,用于输出到控制台'''
        ch = logging.StreamHandler()
        '''设置logger等级'''
        ch.setLevel(logging.DEBUG)
        '''日志输出格式'''
        ch.setFormatter(self.formatter)
        '''给logger添加handler'''
        self.logger.addHandler(ch)
        
        
        if level == 'info':
            self.logger.info(message)
        elif level == 'debug':
            self.logger.debug(message)
        elif level == 'warning':
            self.logger.warning(message)
        elif level == 'error':
            self.logger.error(message)
            
        ''' 这两行代码是为了避免日志输出重复问题'''
        self.logger.removeHandler(ch)
        self.logger.removeHandler(fh)
        '''关闭打开的文件'''
        fh.close()

    def debug(self, message):
        self.__console('debug', message)
    def info(self, message):
        self.__console('info', message)
    def warning(self, message):
        self.__console('warning', message)
    def error(self, message):
        self.__console('error', message)