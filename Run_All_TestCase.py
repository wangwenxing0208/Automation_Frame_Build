# -*- coding: cp936 -*-
#coding:UTF-8
import unittest,os,time,HTMLTestRunner

def allTest():
    '''获取所有要执行的测试用例'''
    suite = unittest.defaultTestLoader.discover(\
            start_dir = os.path.join(os.path.dirname(__file__),'Testcase'), \
            pattern = 'test_*.py', top_level_dir = None)
    return suite

def getNowTime():
    '''获取当前的时间'''
    return time.strftime('%Y-%m-%d %H-%M-%S', time.localtime(time.time()))

def run():
    fileName = os.path.join(os.path.dirname(__file__), 'report',getNowTime() + 'report.html')
    fp = open(fileName, 'wb')
    runner = HTMLTestRunner.HTMLTestRunner(stream = fp, title = 'UI自动化测试报告', description = 'UI自动化测试报告详细信息')
    runner.run(allTest())

if __name__ == "__main__":
    run()
